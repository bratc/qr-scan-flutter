import 'dart:io';

import 'package:flutter/material.dart';
import 'package:qr_scanapp/src/bloc/scans_bloc.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:qr_scanapp/src/models/scan_model.dart';
import 'package:qr_scanapp/src/pages/directions_page.dart';
import 'package:qr_scanapp/src/pages/map_page.dart';
import 'package:qr_scanapp/src/providers/db_provider.dart';
import 'package:qr_scanapp/src/utils/utils.dart' as utils;

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentIndex = 0;
  final scanBloc = new ScansBloc();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('QRScanner'),
        backgroundColor: Theme.of(context).primaryColor,
        actions: <Widget>[
          IconButton(
              onPressed: scanBloc.deleteAllScansStream,
              icon: Icon(Icons.delete_forever))
        ],
      ),
      body: _loadPage(currentIndex),
      bottomNavigationBar: _bottomNavigationBar(context),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).primaryColor,
        onPressed: () => _scannQr(context),
        child: Icon(Icons.filter_center_focus),
      ),
    );
  }

  _scannQr(BuildContext context) async {
    // geo:3.544774083675094,-76.30208387812503
    // https://gitlab.com/bratc
    String scannString = '';
    //String scannString = 'https://gitlab.com/bratc';

    try {
      scannString = await BarcodeScanner.scan();
    } catch (e) {
      scannString = e.toString();
    }

    if (scannString != null) {
      print('SCANN OK...$scannString');
      final scanModel = ScanModel(value: scannString, type: 'http');
      // final scanModel2 = ScanModel(value: 'geo:3.544774083675094,-76.30208387812503', type: 'geo');
      // scanBloc.setScansStream(scanModel2);

      if (scanModel.type == 'other') {
       _showAlertDialog(context, scanModel);
      } else {
        scanBloc.setScansStream(scanModel);
        if (Platform.isIOS) {
          Future.delayed(Duration(milliseconds: 750), () {
            utils.launchURL(context, scanModel);
          });
        } else {
          utils.launchURL(context, scanModel);
        }
      }
    }
  }

  Future<void> _showAlertDialog(BuildContext context, ScanModel scanModel) async {
   await showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
       return AlertDialog(
        title:  Text('Error'),
        content: Column(
          mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text('El código ${scanModel.value} no es un tipo  permitido'),

          ],
         ),
         actions: <Widget>[
           FlatButton(  
             child: Text('Aceptar'),
             onPressed: (){
               Navigator.of(context).pop();
             },
           )
         ],
      );
      }
      );
  }

  Widget _loadPage(int indexPage) {
    switch (indexPage) {
      case 0:
        return MapPage();
        break;
      case 1:
        return DirectionsPage();
        break;
      default:
        return MapPage();
    }
  }

  Widget _bottomNavigationBar(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: currentIndex,
      onTap: (index) {
        setState(() {
          this.currentIndex = index;
        });
      },
      items: [
        BottomNavigationBarItem(icon: Icon(Icons.map), title: Text('Maps')),
        BottomNavigationBarItem(icon: Icon(Icons.scanner), title: Text('Scan')),
      ],
    );
  }
}
