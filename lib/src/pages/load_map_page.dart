


import 'package:flutter/material.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:qr_scanapp/src/providers/db_provider.dart';

class LoadMapPage extends StatefulWidget {
  @override
  _LoadMapPageState createState() => _LoadMapPageState();
}

class _LoadMapPageState extends State<LoadMapPage> {
  final map = MapController();
  String typeMap = 'streets';
  @override
  Widget build(BuildContext context) {
      final ScanModel scanModel = ModalRoute.of(context).settings.arguments;
      print('data model router ${scanModel.value}');
    return Scaffold( 
      appBar: AppBar(
        title: Text('Load Map'),
        actions: <Widget>[
          IconButton( 
            icon: Icon(Icons.my_location),
            onPressed: (){
              map.move(scanModel.getLatLong(), 15);
            },
          )
        ],
      ),
      body: _createMap(scanModel),
      floatingActionButton: _createButtonChangeTypeMap(context),
    );
  }

  Widget _createButtonChangeTypeMap(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.repeat),
      backgroundColor: Theme.of(context).primaryColor,
      onPressed: () {
      
        if (typeMap == 'streets') {
          typeMap = 'dark';
        } else if (typeMap == 'dark') {
          typeMap = 'light';
        
        } else if (typeMap == 'light') {
          typeMap = 'outdoors';
        
        } else if (typeMap == 'outdoors') {
          typeMap = 'satellite';
        } else {
          typeMap = 'streets';
        }
        print('change Typemap $typeMap');
        setState((){
         
        });
      },
    );
  }

  Widget _createMap(ScanModel scanModel) {
    return FlutterMap(
      mapController: map,
      options: MapOptions(
        center: scanModel.getLatLong(),
        zoom: 10
        ),  
        layers: [
          _createMapLayer(),
          _createMarker(scanModel)
        ],
      );
  }

  _createMapLayer() {
    return TileLayerOptions(
      urlTemplate: 'https://api.mapbox.com/v4/'
      '{id}/{z}/{x}/{y}@2x.png?access_token={accessToken}',
      additionalOptions: {
        'accessToken': 'pk.eyJ1IjoiYnJhdGMiLCJhIjoiY2s4dDZpNzBtMDNnYTNlcXRiemFzaGk4YSJ9.nsuM1P8DL14a8tkIMSTcAQ',
        'id': 'mapbox.$typeMap'
      }
    );
  }

  _createMarker(ScanModel scanModel) {
    return MarkerLayerOptions( 
        markers: <Marker> [
          Marker( 
            width: 100.0,
            height: 100.0,
            point: scanModel.getLatLong(),
            builder: (context) => Container( 
              child: Icon(
                Icons.location_on,
                size: 70.0,
                color: Theme.of(context).primaryColor
              ),
            )
           )
        ]
     );
  }
}