import 'package:flutter/material.dart';
import 'package:qr_scanapp/src/bloc/scans_bloc.dart';
import 'package:qr_scanapp/src/models/scan_model.dart';
import 'package:qr_scanapp/src/providers/db_provider.dart';
import 'package:qr_scanapp/src/utils/utils.dart' as utils;

class DirectionsPage extends StatelessWidget {
  final scansBloc = new ScansBloc();
  @override
  Widget build(BuildContext context) {
    scansBloc.getScansStream();
    return StreamBuilder<List<ScanModel>>( 
      stream: scansBloc.getScansControllerStreamHttp,
      builder: (BuildContext context, AsyncSnapshot<List<ScanModel>> snapshot) {
        if(!snapshot.hasData) {
         return Center(child: CircularProgressIndicator());
        }

        final scans = snapshot.data;
        if(scans.length <= 0) {
          return Center(child: Text('No existen registros'));
        }

        return ListView.builder(
          itemCount: scans.length, // TODO NO OLVIDAR EL itemCount en un ListView
          itemBuilder: (context, i) => Dismissible( 
            key: UniqueKey(),
            background: Container(color: Colors.orange),
            onDismissed: (direction) => scansBloc.deleteScansStream(scans[i].id),
            child: ListTile( 
              leading: Icon(Icons.cloud_queue, color: Theme.of(context).primaryColor),
              title: Text(scans[i].value),
              trailing: Icon(Icons.keyboard_arrow_right, color: Colors.grey),
              onTap: () =>  utils.launchURL(context, scans[i])
              
            ),
          )
        );
        
      }
    );
  }
}