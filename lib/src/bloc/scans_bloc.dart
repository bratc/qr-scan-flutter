
import 'dart:async';

import 'package:qr_scanapp/src/bloc/validators.dart';
import 'package:qr_scanapp/src/providers/db_provider.dart';

class ScansBloc with Validatos {

  static final ScansBloc _singleton = new ScansBloc._internal();

  factory ScansBloc() {
    return _singleton;
  }

  ScansBloc._internal() {
    // Obtener Scans de la base de datos
  }

  final _scansControllerStream = StreamController<List<ScanModel>>.broadcast();

  Stream<List<ScanModel>> get getScansControllerStream => _scansControllerStream.stream.transform(validateGeo);
  Stream<List<ScanModel>> get getScansControllerStreamHttp => _scansControllerStream.stream.transform(validateHttp);

  dispose() {
    _scansControllerStream?.close();
  }

  getScansStream() async {
    _scansControllerStream.sink.add(await DBProvider.db.getAllScans());
  }

  setScansStream(ScanModel scanModel) async {
    await DBProvider.db.newScan(scanModel);
    getScansStream();
  }
  
  deleteScansStream(int id) async {
    await DBProvider.db.deleteScan(id);
    getScansStream();
  }

  deleteAllScansStream() async {
    await DBProvider.db.deleteAllScans();
    getScansStream();
  }

}