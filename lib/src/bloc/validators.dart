
import 'dart:async';

import 'package:qr_scanapp/src/providers/db_provider.dart';

class Validatos {
  final validateGeo = StreamTransformer<List<ScanModel>, List<ScanModel>>.fromHandlers( 
    handleData: (scans, sink) {
      final geoScans = scans.where((s) => s.type == 'geo').toList();
      sink.add(geoScans);
    }
  );
  final validateHttp = StreamTransformer<List<ScanModel>, List<ScanModel>>.fromHandlers( 
    handleData: (scans, sink) {
      final httpScans = scans.where((s) => s.type == 'http').toList();
      sink.add(httpScans);
    }
  );
}