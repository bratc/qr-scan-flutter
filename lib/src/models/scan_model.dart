
import 'package:latlong/latlong.dart';

class ScanModel {
    int id;
    String type;
    String value;

    ScanModel({
        this.id,
        this.type,
        this.value,
    }) {
      if (this.value.contains('http')) {
        this.type = 'http';
      } else if (this.value.contains('geo')) {
        this.type = 'geo';
      } else {
        this.type = 'other';
      }
    }

    factory ScanModel.fromJson(Map<String, dynamic> json) => ScanModel(
        id    : json["id"],
        type  : json["type"],
        value : json["value"],
    );

    Map<String, dynamic> toJson() => {
        "id"    : id,
        "type"  : type,
        "value" : value,
    };

    LatLng getLatLong() {
      final latLong = value.substring(4).split(',');
      final lat = double.parse(latLong[0]);
      final long = double.parse(latLong[1]);
      return LatLng(lat, long); 
    }
}
