import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:qr_scanapp/src/models/scan_model.dart';
export 'package:qr_scanapp/src/models/scan_model.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

// Implementar patron singleton .. => mantener una sola instancia de la base de datos
class DBProvider {
  static Database _database;
  static final DBProvider db = DBProvider._();

  DBProvider._();

  Future<Database> get database async {
    if(_database != null) {
      return _database;
    }

    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentDirectory.path, 'ScansDB.db');
    return await openDatabase(
      path,
      version: 1,
      onOpen: (db) {},
      onCreate: (Database db, int version) async {
        await db.execute(
          'CREATE TABLE Scans ('
          'id INTEGER PRIMARY KEY, '
          'type TEXT, '
          'value TEXT'
          ')'
        );
      }
    );
  }

// Insert Manual en Sqlite
  newScanRaw( ScanModel scanModel) async {
    final db = await database;

    final res = await db.rawInsert(
      "INSET Into Scans(id, type, value) "
      "VALUES(${scanModel.id}, '${scanModel.type}', '${scanModel.value}')"
      );
      return res;
  }
// Insert directo con el model usando el metodo toJson()
 Future<int> newScan(ScanModel scanModel) async {
    final db  = await database;
    final res = await db.insert('Scans', scanModel.toJson());
    return res;
  }

  // SELECT SQLite

  Future<ScanModel> getScanId(int id) async {
    final db  = await database;
    final res = await db.query('Scans', where: 'id = ?', whereArgs: [id]);
    return res.isNotEmpty ? ScanModel.fromJson(res.first): null; 
  }

  Future<List<ScanModel>> getAllScans() async {
    final db  = await database;
    final res = await db.query('Scans');
    List<ScanModel> listScanModel = res.isNotEmpty ? res.map((scan) => ScanModel.fromJson(scan)).toList() : [];
    return listScanModel;
  }

  Future<List<ScanModel>> getAllScansByType(String type) async {
    final db  = await database;
    final res = await db.rawQuery("SELECT * FROM Scans WHERE type='$type'");
    List<ScanModel> listScanModel = res.isNotEmpty ? res.map((scan) => ScanModel.fromJson(scan)).toList() : [];
    return listScanModel;
  }

  // UPDATE

 Future<int> updateScan(ScanModel scanModel) async {
    final db  = await database;
    final res = await db.update('Scans', scanModel.toJson(), where: 'id = ?', whereArgs: [scanModel.id]);
    return res;
  }

Future<int> deleteScan( int id )async {
   final db  = await database;
   final res = await db.delete('Scans', where: 'id = ?', whereArgs: [id]);
   return res;
 } 

Future<int> deleteAllScans() async {
  final db   = await database;
   final res = await db.rawDelete("DELETE FROM Scans");
   return res;
} 
}