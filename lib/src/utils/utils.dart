import 'package:flutter/widgets.dart';
import 'package:qr_scanapp/src/models/scan_model.dart';
import 'package:url_launcher/url_launcher.dart';


launchURL(BuildContext context, ScanModel scanModel) async {
  
  if(scanModel.type == 'http') {
    print('value url ${scanModel.value}');
  if (await canLaunch(scanModel.value)) {
    await launch(scanModel.value);
  } else {
    throw 'Could not launch ${scanModel.value}';
  }
  } else {
    print('GEO...');
          Navigator.pushNamed(context, 'loadMap', arguments: scanModel);
  }
}
