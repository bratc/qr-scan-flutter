import 'package:flutter/material.dart';
import 'package:qr_scanapp/src/pages/home_page.dart';
import 'package:qr_scanapp/src/pages/load_map_page.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      
      title: 'QR Scanner',
      initialRoute: 'home',
      routes: {
          'home': (BuildContext context) => HomePage(),
          'loadMap': (BuildContext context) => LoadMapPage()
        },
      theme: ThemeData(
        primaryColor: Colors.deepOrangeAccent
       ),
    );
  }
}